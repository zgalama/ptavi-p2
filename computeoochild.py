#!/usr/bin/python3
# -*- coding: utf-8 -*-

import computeoo


class ComputeChild(computeoo.Compute):  # llamamos a la clase del programa computeoo

    def set_def(self, num2):
        if num2 is None:  # si num2 no ha sido especificado
            return self.default  # num2 será el valor por defecto
        else:
            if num2 <= 0:  # si num2 para la base de log es menor o igual a 0
                raise ValueError('Error: third argument should be a number')  # saldrá este mensaje
            else:
                return num2

    def get_def(self):
        return self.default  # imprime el valor por defecto


val_def = ComputeChild()  # instanciamos un objeto para llamar la función


print('El valor por defecto utilizado (en caso de) es:', val_def.get_def())
