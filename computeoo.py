#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math
import sys


class Compute:

    def __init__(self): # inicimos con el init al deber ser el primero cuando creamos una clase
        self.default = 2 #con esto definimos que la potencia o log sea 2 en el programa

    def power(self, num, exp=2):
            return num ** exp

    def log(self, num, base=2):
            return math.log(num, base)

#cuerpo del programa original, no modifico nada
if len(sys.argv) < 3:
    sys.exit("Error: at least two arguments are needed")

try:
    num = float(sys.argv[2])
except ValueError:
    sys.exit("Error: second argument should be a number")

if len(sys.argv) == 3:
    num2 = 2
else:
    try:
        num2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: third argument should be a number")

if sys.argv[1] == "power":
    potencia = Compute() # definimos el objeto y la asociamos con la clase
    result = potencia.power(num, num2) # llamamos al objeto y a la funcion dentro de la clase
elif sys.argv[1] == "log":
    logaritmo = Compute()
    result = logaritmo.log(num, num2)
else:
    sys.exit('Operand should be power or log')

print(result)
