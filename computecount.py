#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math


class Compute:
    count = 0

    def __init__(self):
        self.default = 2

    def power(self, num, exp=2):
        self.count += 1
        return num ** exp


    def log(self, num, base=2):
        self.count += 1
        return math.log(num, base)  

    def set_def(self, num2):
        self.count += 1
        if num2 is None:  # si num2 no ha sido especificado
            return self.default  # num2 será el valor por defecto
        else:
            if num2 <= 0:  # si num2 para la base de log es menor o igual a 0
                raise ValueError('Error: third argument should be a number')  # saldrá este mensaje
            else:
                return num2
    def get_def(self):
        self.count += 1
        return(self.default)  # imprime el valor por defecto

